import java.io.FileInputStream;

public class ReadFile {
	public static void main (String[] args){
		String fname;
		fname = "/media/VM/debian/hd1.vdi";
		//fname = "/home/user/VirtualBox VMs/debian/hd1.vdi";

		int bs = 1024 * 128;
		byte[] buffer = new byte[bs];

		try {
			FileInputStream is = new FileInputStream( fname );
			int bytesRead = is.read( buffer );
			int lastBytesRead = -2;
			while( bytesRead!=-1 ){
				lastBytesRead = bytesRead;
				bytesRead = is.read( buffer );
			}
			System.out.println( "last bytes read " + lastBytesRead );
		} catch ( Exception ex ){
			System.out.println( "ERROR: " + ex.getMessage() );
		}

	}
}
