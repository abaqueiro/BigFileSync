#!/usr/bin/php
<?php

/*

PROBLEM:

on 32bit kernel the php compiles with 32bit integer
so is only posible to open 2Gb files max
to open large files you require to run php in 64bits mode

*/

$fname = "/media/VM/debian/hd1.vdi";
$fname = "/home/user/VirtualBox VMs/debian/hd1.vdi";
$fname = "/home/user/VirtualBox VMs/ubuntu10.04/swap.vdi";

$bs = 1024 * 128; // 128 k

$fh = fopen( $fname, 'r' );
if ( $fh===FALSE ){
	echo "ERROR opening file [$fname].\n";
	return;
}
$buffer = null;
echo "BEGIN read.\n";
$cc=0;
while ( !feof($fh) ){
	$buffer = fread( $fh, $bs );
	$cc++;
}
echo "$cc readings.\n";
echo "END read.\n";

?>
